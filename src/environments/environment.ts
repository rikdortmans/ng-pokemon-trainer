// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL:"https://rd-noroff-assignment-api.herokuapp.com/trainers",
  API_KEY:"szmgwlxulbeujbedhkuktmhjhlaogrhi",
  
  API_POKE_URL: "https://pokeapi.co/api/v2/pokemon?limit=10000&offset=0",
  API_POKE_IMG_URL: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",
  API_POKEBALL_IMG_URL: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
