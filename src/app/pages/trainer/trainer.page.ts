import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss']
})
export class TrainerPage implements OnInit {
  user?: User;

  constructor(private userService: UserService) {
    if (this.userService.getUser) {
      this.user = this.userService.getUser;
    }
  }

  ngOnInit(): void {

  }

  /**
   * Function is Output from Trainer Component
   * Filters out the removed Pokemon from the Pokemon on the User
   * Will update the user with the a new array without the removed Pokemon
   * @param pokemon Pokemon selected to be removed
   */
  removePokemon(pokemon: Pokemon) {
    if (this.userService.getUser) {
      const updatedPokemon = this.userService.getUser.pokemon.filter(p => p !== pokemon.name);
      this.userService.getUser.pokemon = updatedPokemon;
      this.userService.setUser = this.userService.getUser;
      this.userService.updateUser(this.userService.getUser);
    }
  }

  /**
   * Function is Output from Trainer Component
   * Clears all Pokemon on the user by replacing the value with an empty array
   * Update the User on the UserService
   */
  removeAllPokemon() {
    if (this.userService.getUser) {
      this.userService.getUser.pokemon = [];
      this.userService.setUser = this.userService.getUser;
      this.userService.updateUser(this.userService.getUser);
    }

  }
}
