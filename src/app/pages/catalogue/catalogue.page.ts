import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

const { API_POKEBALL_IMG_URL } = environment;

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.scss']
})
export class CataloguePage implements OnInit {
  public loading: boolean = false;
  public error: string = "";

  pokemon: Pokemon[] = [];
  caughtPokemon: string[] = [];

  constructor(private pokemonService: PokemonService, private userService: UserService) { }

  ngOnInit(): void {
    this.loading = true;
    // User can possibly be undefined, check before using
    if (this.userService.getUser) {
      this.caughtPokemon = this.userService.getUser?.pokemon;
    }

    this.checkForCaughtPokemon();
    this.loading = false;
  }

  /**
   * Function loops through every pokemon stored in session
   * For every one it will compare the name against the names of the caught Pokemon
   * Goes through session pokemon first because these are Objects instead of strings.
   * And thus have the caught boolean and the pokeball image variables.
   */
  private checkForCaughtPokemon() {
    this.pokemon = this.pokemonService.pokemon;
    this.pokemon.forEach(mon => {
      mon.caught = false;
      this.caughtPokemon.forEach(caughtMon => {
        if (mon.name === caughtMon) { mon.caught = true; }
      });
      mon.ballImg = API_POKEBALL_IMG_URL;
    });
  }

  /**
   * Finds the selected pokemon by id
   * If it exists, it will update the userService with the newly added Pokemon on the User
   * Rechecks the CaughtPokemon in order to update HTML
   * @param pokemon Pokemon selected by the user
   */
  addCaughtPokemon(pokemon: Pokemon) {
    let selectedPokemon = this.pokemon.find(p => p.id === pokemon.id);
    if (selectedPokemon) {
      selectedPokemon.caught = true;
      this.caughtPokemon.push(selectedPokemon.name);
      if (this.userService.getUser) {
        this.userService.setUser = this.userService.getUser;
        this.userService.updateUser(this.userService.getUser);
      }
    }
    this.checkForCaughtPokemon();
  }
}