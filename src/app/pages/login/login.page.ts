import { HttpErrorResponse } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})

export class LoginPage implements OnInit {
  public loading: boolean = false;
  public error: string = "";
  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private router: Router,
    private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.pokemonService.fetchPokemon();
  }

  /**
   * First uses the login method on the LoginService to check if the user exists in API.
   * If user does not exist, it will create a new User using the UserService.
   * If either of the login or create return a valid user object it will store 
   * the user in local storage and navigate to catalogue page.
   * @param form The Login Form from login.page.html
   */
  public onLoginSubmit(form: NgForm) {
    this.loading = true;
    const { username } = form.value;
    this.loginService.login(username).subscribe({
      next: (user: User | undefined) => {
        if (user === undefined) { 
          this.userService.createUser(username)?.subscribe({
            next: (user: User | undefined) => {
              if (user === undefined) { this.error = "Something went wrong creating a user, try again." }
              else { this.setUserAndNavigate(user); }
            }
          })
        }
        else { this.setUserAndNavigate(user); }
      },
      error: (error) => { this.error = error },
      complete: () => { this.loading = false }
    })
  }

  private setUserAndNavigate(user: User) {
    this.userService.setUser = user;
    this.router.navigateByUrl("/catalogue");
  }
}