
export interface Pokemon {
    id:number;
    name: string;
    url: string;
    image:string;
    caught:boolean;
    ballImg:string;
}

export interface PokemonResponse {
    count: number;
    next: string;
    previous: string;
    results: Pokemon[];
}

export interface TrainerResponse{
    id:number;
    username:string,
    pokemon:Pokemon[]
}