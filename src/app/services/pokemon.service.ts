import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { STORAGE_KEY_POKEMON } from '../models/storage.model';
import { StorageService } from './storage.service';

const { API_POKE_IMG_URL } = environment;
const { API_POKE_URL } = environment;

@Injectable({
  providedIn: 'root'
})

export class PokemonService implements OnInit {

  private _pokemon: Pokemon[] = [];

  /**
   * Property will check the local session storage for Pokemon.
   * If Storage has pokemon, it will return.
   * Else it will retrieve pokemon from API then return
   * @returns An array of Pokemon Objects
   */
  public get pokemon(): Pokemon[] {
    try {
      if (this._pokemon.length == 0) {
        if (this.storage.getStorage(STORAGE_KEY_POKEMON) !== "") {
          this._pokemon = JSON.parse(this.storage.getStorage(STORAGE_KEY_POKEMON))
        }
        else {
          this.fetchPokemon();
        }
      }
      return this._pokemon;
    } catch (error) {
      console.error(error);
    }
    return this._pokemon;
  }

  constructor(private http: HttpClient, private storage: StorageService) { }

  ngOnInit(): void {

  }

  private getImageUrl(url: string): string {
    const id = Number(url.split('/').filter(Boolean).pop());

    return `${API_POKE_IMG_URL}/${id}.png`;
  }

  /**
   * Function the retrieve Pokemon from API or Storage
   * and store them in the Service.
   * Is called by Login as preparation and used by get pokemon property
   */
  public fetchPokemon() {
    try {
      if (this.storage.getStorage(STORAGE_KEY_POKEMON) != "") {
        this._pokemon = JSON.parse(this.storage.getStorage(STORAGE_KEY_POKEMON));
      }

      if (this._pokemon.length == 0) {
        this.getPokemon().subscribe((results: Pokemon[]) => {
          this._pokemon = results
          this.storage.setStorage(STORAGE_KEY_POKEMON, JSON.stringify(this._pokemon))
        })
      }
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Private method to retrieve pokemon from API
   * @returns Array of Pokemon objects with images
   */
  private getPokemon(): Observable<Pokemon[]> {
    return this.http.get<PokemonResponse>(API_POKE_URL)
      .pipe(
        map((response: PokemonResponse) => {
          const results = response.results;
          if (results) {
            for (let i = 0; i < results.length; i++) {
              let mon = results[i];
              mon.id = i;
              mon.image = this.getImageUrl(mon.url);
              results[i] = mon;
            }
          }
          return results;
        }))
  }
}
