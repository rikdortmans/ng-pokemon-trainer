import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {
  }

  public setStorage(key: string, value: string) {
    sessionStorage.setItem(key, value);
  }

  public getStorage(key: string) {
    const value = sessionStorage.getItem(key);
    if (value) {
      return value;
    }
    else {
      return "";
    }
  }

  public setLocal(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  public getLocal(key: string) {
    const value = localStorage.getItem(key);
    if (value) {
      return value;
    }
    else {
      return "";
    }
  }
}
