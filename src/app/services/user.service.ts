import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { nanoid } from 'nanoid';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { STORAGE_KEY_USER } from '../models/storage.model';
import { User } from '../models/user.model';
import { StorageService } from './storage.service';

const { API_URL } = environment;
const { API_KEY } = environment;


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user?: User;
  public get getUser(): User | undefined {
    if (!this.user) {
      if (this.storage.getLocal(STORAGE_KEY_USER) !== "") {
        this.user = <User>JSON.parse(this.storage.getLocal(STORAGE_KEY_USER));
        if (this.user) { return this.user; }
      }
      else {
        return undefined;
      }
    }
    return this.user;
  }

  public set setUser(v: User | undefined) {
    if (v !== undefined) {
      this.storage.setLocal(STORAGE_KEY_USER, JSON.stringify(v))
    }
    this.user = v;
  }

  constructor(private storage: StorageService, private http: HttpClient) {
  }

  public createUser(username: string): Observable<User> | undefined {
    const body = {
      id: nanoid,
      username: username,
      pokemon: []
    }
    const headers = {
      'Content-Type': 'application/json',
      'x-api-key': API_KEY,
    }
    return this.http.post<User>(`${API_URL}`, body, { headers })
      .pipe(
        map((response: User) => response)
      )
  }

  public updateUser(user: User) {
    const body = {
      id: user.id,
      username: user.username,
      pokemon: user.pokemon,
    }
    const headers = {
      'Content-Type': 'application/json',
      'x-api-key': API_KEY,
    }

    try {
      this.http.patch<User>(`${API_URL}/${user.id}`, body, { headers })
        .subscribe({
          next: data => {
            console.log(data);
          },
          error: error => {
            console.error(error.message);
          }
        });
    } catch (error) {
      console.error(error);
    }
  }
}
