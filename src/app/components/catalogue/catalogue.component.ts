import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  @Input()
  pokemon: Pokemon[] = [];

  @Input()
  caughtPokemon: string[] = [];

  // Variables for Paginate
  page: number = 1;
  count: number = 0;
  pageSize = 50;
  tableSizes: any = [10, 20, 50, 100];

  ngOnInit(): void { }

  @Output() addPokemon = new EventEmitter<Pokemon>();

  /**
   * Retrieves the selected pokemon from the pokemon object []
   * If this pokemon does not exist in the caught pokemon []
   * It will emit an event to the parent in order to add it to the array
   * Else it will alert the user
   * @param id Id of the Pokemon to find
   */
  handleCardClick(id: number) {
    const pokemon = this.pokemon.find(p => p.id === id);
    if (pokemon) {
      if (!this.caughtPokemon.find(p => p === pokemon.name)) {
        this.addPokemon.emit(pokemon);
      }
      else { alert("Pokemon already caught!"); }
    }
  }

  onTableSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
  }
  onTableDataChange(event: any) {
    this.page = event;
  }
}
