import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaughtpokemonComponent } from './caughtpokemon.component';

describe('CaughtpokemonComponent', () => {
  let component: CaughtpokemonComponent;
  let fixture: ComponentFixture<CaughtpokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaughtpokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaughtpokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
