import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { STORAGE_KEY_POKEMON } from 'src/app/models/storage.model';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-caughtpokemon',
  templateUrl: './caughtpokemon.component.html',
  styleUrls: ['./caughtpokemon.component.scss']
})
export class CaughtpokemonComponent implements OnInit {

  @Input()
  userPokemon: string[] = [];

  caughtPokemon: Pokemon[] = [];
  public loading: boolean = false;
  public error: string = "";

  constructor(private storage: StorageService) { }

  /**
   * Gets all pokemon from Session storage
   * If these exist and the user has pokemon as well
   * It will compare the names of the users pokemon against the names of all others using find()
   * If a match is found, the found Pokemon is added to the Caught Pokemon array
   */
  ngOnInit(): void {
    this.loading = true;
    const allPokemon: Pokemon[] = JSON.parse(this.storage.getStorage(STORAGE_KEY_POKEMON));
    if (allPokemon && this.userPokemon) {
      this.userPokemon.forEach(userPokemon => {
        const pokemonFound = allPokemon.find(p => p.name === userPokemon);
        if (pokemonFound) { this.caughtPokemon.push(pokemonFound) }
      });
    }
    else { this.error = "Error retrieving Pokemon. Please refresh" }
    this.loading = false;
  }

  @Output() removePokemon = new EventEmitter<Pokemon>();
  @Output() removeAllPokemon = new EventEmitter();

  /**
   * Filters out the removed pokemon from the local caught pokemon array
   * In order to trigger a component refresh
   * Will emit an event to parent to trigger the rest
   * @param pokemon The Pokemon to be removed
   */
  handleReleaseClick(pokemon: Pokemon) {
    const keepPokemon = this.caughtPokemon.filter(p => p.name !== pokemon.name);
    this.caughtPokemon = keepPokemon;
    pokemon.caught = false;
    this.removePokemon.emit(pokemon)
  }

  handleReleaseAllClick() {
    this.caughtPokemon = [];
    this.removeAllPokemon.emit();
  }
}
